#!/bin/sh

# This file is a part of Place-Name Props, a collection of supplementary files
# for the Berwickshire Place-Name Resource project. See the accompanying file
# "COPYING" for the copyright and licensing information.

MYDIR=$(dirname "$(realpath "$0")")
SCRIPTDIR="$MYDIR/GeomorphonProfiler"
# shellcheck disable=SC1090
. "$SCRIPTDIR/init.sh"
assert_nonempty_file "$INPUTFILE"
assert_dir "$JSONDIR"
assert_dir "$PNGDIR"
CSVFILE="$ELEVDIR/$SE_SK_FL_DI/export.csv"
PDFFILE="$ELEVDIR/$SE_SK_FL_DI/export.pdf"
# shellcheck source=./pnplib.sh
. "$MYDIR/pnplib.sh"

gen_csv_file()
{
	(
		printf '\357\273\277'  # Prepend a UTF-8 BOM.
		"$MYDIR/generate_table_csv.py" \
			--input="$INPUTFILE" \
			--jsondir="$JSONDIR" \
			--output=-
	) > "${1:?}"
}

# EXPORT_PDF_OPTIONS is intended to expand.
# shellcheck disable=SC2086
gen_pdf_file()
{
	"$MYDIR/generate_atlas_pdf.py" \
		--input="$INPUTFILE" \
		--jsondir="$JSONDIR" \
		--pngdir="$PNGDIR" \
		$EXPORT_PDF_OPTIONS \
		--output="${1:?}"
}

produce_file gen_csv_file "$CSVFILE" || exit 2
produce_file gen_pdf_file "$PDFFILE" || exit 2
