#!/usr/bin/env python3

# This file is a part of Place-Name Props, a collection of supplementary files
# for the Berwickshire Place-Name Resource project. See the accompanying file
# "COPYING" for the copyright and licensing information.

import argparse
import sys
import os
import re
import csv
from collections import OrderedDict
from threading import Thread, BoundedSemaphore
import pnplib
from GeomorphonProfiler.gplib import session, load_profile_or_exit


CSSTEXT = """
body {
	font-family: "Liberation Sans";
}

/* wkhtmltopdf disregards all its page margin options. */
div.page {
	margin-top: 2.5cm; /* 29mm in the hard copy*/
	page-break-inside: avoid;
}

div.footer {
	text-align: right;
	margin-top: 1cm;
	margin-right: 1.5cm;
}

div.pagebreak {
	page-break-after: always;
}

h1 {
	text-align: center;
}

table {
	border-collapse: collapse;
}

table.entry {
	border: 2px solid #376092;
	border-spacing: 0px;
	margin-left: 2.5cm; /* 30mm in the hard copy */
	margin-right: 1.5cm; /* 22mm in the hard copy */
	margin-bottom: 2mm;
}

table.entry td {
	vertical-align: top;
}

table.filler td {
	vertical-align: initial;
}

table.filler {
	width: 100%;
}

tr.entry_header, td.dataval {
	background: #e8eef8;
}

tr.entry_header {
	font-size: 14pt;
	font-weight: bold;
}

tr.entry_header, td.dataname, span.srmarker, span.reels_hdr, div.section_name,
div.section_code  {
	color: #376092;
}

span.reels_hdr {
	font-style: italic;
}

td.entry_title {
	text-transform: uppercase;
	font-style: italic;
}

td div {
	margin-left: 1ch;
	margin-right: 1ch;
}

td.dataval {
	font-size: 13pt;
}

td.dataname, td.dataval {
	text-align: right;
}

td.dataval.str {
	text-align: initial;
	background: initial;
}

td.dataname {
	font-size: 11pt;
	font-style: italic;
}

span.element {
	white-space: nowrap;
}

span.lang {
	vertical-align: sub;
	font-size: smaller;
}

span.element span.term {
	font-style: italic;
}

span.sense {
	vertical-align: super;
	font-size: smaller;
}

span.target span.term {
	font-weight: bold;
}

div.part_name, div.section_name, div.section_code {
	text-align: center;
	font-weight: bold;
}

div.part_name {
	margin-top: 10.5cm;
	font-size: 16pt;
}

div.section_name {
	margin-top: 13mm;
	font-size: 24pt;
}

div.section_code {
	font-size: 16pt;
}

td.image {
	width: 10%;
}

img.geovis {
	/* The image comes out of the printer 0.764 times the requested size. */
	width: 92mm;
	height: 92mm;
}
"""


ap = argparse.ArgumentParser ()
ap.add_argument (
	'--input',
	metavar = 'FILE',
	nargs = '?',
	type = argparse.FileType ('r'),
	default = sys.stdin,
	help = 'file to read the CSV data from ("-" for stdin)')
ap.add_argument (
	'--jsondir',
	metavar = 'DIR',
	required = True,
	type = str,
	help = 'the directory with JSON profiles')
ap.add_argument (
	'--pngdir',
	metavar = 'DIR',
	required = True,
	type = str,
	help = 'the directory with PNG images')
ap.add_argument (
	'--first-page-no',
	metavar = 'NUMBER',
	type = int,
	default = 1,
	help = 'the first page number')
ap.add_argument (
	'--max-procs',
	metavar = 'NUMBER',
	type = int,
	default = 1,
	help = 'maximum parallel wkhtmltopdf processes')
ap.add_argument (
	'--pages-per-proc',
	metavar = 'NUMBER',
	type = int,
	default = 25,
	help = 'maximum number of pages per each wkhtmltopdf process')
ap.add_argument (
	'--output',
	metavar = 'FILE',
	required = True,
	type = str,
	help = 'file to write the PDF output to')
args = ap.parse_args()
PNGDIR = os.path.abspath (args.pngdir)
JSONDIR = os.path.abspath (args.jsondir)
ENTRIES_PER_PAGE = 3
PAGES_PER_PROC = args.pages_per_proc


def gen_entry (rd: dict, jd: dict) -> str:
	def whole (x: float) -> str:
		return format (round (x), ',')

	def format_name (s: str) -> str:
		return re.sub ('(\w)([1-6]?{|[1-6])', r'\1<sub>\2</sub>', s)

	def simple_td (tdclass: str, text: str, **kwargs: dict) -> str:
		ret = '<TD'
		kwargs['class'] = tdclass
		for key in kwargs.keys():
			ret += ' %s="%s"' % (key, kwargs[key])
		ret += '><DIV>%s</DIV></TD>' % text
		return ret

	from pnplib import IMPORTCSV, EXPORTCSV
	SMARKER = '<SPAN class="srmarker">%s</SPAN>' % pnplib.SMARKER
	RMARKER = '<SPAN class="srmarker">%s</SPAN>' % pnplib.RMARKER
	if rd['s_name'] != '':
		entry_title = format_name (rd['s_name'])
		footer_str = '%s %s' % (RMARKER, format_name (rd['r_name']))
		left_str = '%s %s' % (SMARKER, rd['ngr9_string'])
		right_str = '%s %s' % (RMARKER, rd['ngr0_string'])
	else:
		entry_title = format_name (rd['r_name'])
		footer_str = '&nbsp;'
		left_str = '%s %s' % (RMARKER, rd['ngr0_string'])
		right_str = '&nbsp;'
	if rd['etymology'] == '':
		etymology_str = '&nbsp;'
		target = pnplib.EnglishTargetElement (rd['element'])
		target_str = target.to_html()
	else:
		target = pnplib.ScotsTargetElement (rd['element'])
		target_str = target.to_html()
		etymology = pnplib.ScotsEtymology (rd['etymology'], target)
		etymology_str = etymology.to_html()
	length = jd['final_results']['elongation'] * jd['final_results']['width_m']
	img_html = '<IMG class="geovis" src="file://%s/%s.png">' % (
		PNGDIR, rd['name'])
	coapp = rd['coapp'].replace ('u', '<sup>u</sup>')
	r_elev = jd['intermediate_data']['origin_elevation_m']
	distance, gradient, s_elev = pnplib.dist_grad_s_elev (rd, r_elev)
	distance_str = '&nbsp;' if distance is None else whole (distance)
	s_to_r_grad = '&nbsp;' if gradient is None else whole (gradient)
	s_elev_str = '&nbsp;' if s_elev is None else whole (s_elev)
	p = pnplib.prominence (jd['final_results'])
	prominence = '&nbsp;' if p is None else whole (p)
	r_class = rd['class'] if rd['class'] != '' else '&nbsp;'
	geom_str = '%s %02u %04u %04u' % (
		jd['final_results']['landform_code'],
		jd['final_results']['landform_cat'],
		jd['intermediate_data']['ternary_498'],
		jd['intermediate_data']['ternary_6561'],
	)
	if rd['pid'] != '':
		pid_str = '<SPAN class="reels_hdr">%s:</SPAN> %s' % \
			(IMPORTCSV['pid'], rd['pid'])
	else:
		pid_str = '&nbsp;'
	fileid = re.sub ('^([0-9]+)_.+$', r'\1', rd['name'])
	tmp = [
		'<TABLE class="entry">',
		'<TR>',
		'<TD colspan=2>',
		'<TABLE class="filler">',
		'<TR class="entry_header">',
		simple_td ('entry_title', entry_title, width = '85%'),
		simple_td ('dataval', rd['parish']),
		'</TR>',
		'</TABLE>',  # end of the top row
		'</TD>',
		'</TR>',
		'<TR>',
		'<TD class="image">%s</TD>' % img_html,
		'<TD>',
		'<TABLE class="filler">',
		'<TR>',
		'<TD colspan=3>',
		'<TABLE class="filler">',
		'<TR>',
		simple_td ('dataval str', left_str, width = '50%'),
		simple_td ('dataval str', right_str, width = '50%'),
		'</TR>',
		'</TABLE>',  # end of the 2-column irregular row table
		'</TD>',
		'</TR>',
		'<TR>',
		simple_td ('dataval str', etymology_str, colspan = 3),
		'</TR>',
		'<TR>',
		'<TD colspan=3>&nbsp;</TD>',
		'</TR>',
		'<TR>',
		simple_td ('dataname', EXPORTCSV['length'], width = '33%'),
		simple_td ('dataname', EXPORTCSV['octa_per'], width = '33%'),
		simple_td ('dataname', EXPORTCSV['mesh_per']),
		'</TR>',
		'<TR>',
		simple_td ('dataval', whole (length)),
		simple_td ('dataval', whole (jd['final_results']['octagon_perimeter_m'])),
		simple_td ('dataval', whole (jd['final_results']['mesh_perimeter_m'])),
		'</TR>',
		'<TR>',
		simple_td ('dataname', EXPORTCSV['width']),
		simple_td ('dataname', EXPORTCSV['octa_area']),
		simple_td ('dataname', EXPORTCSV['mesh_area']),
		'</TR>',
		'<TR>',
		simple_td ('dataval', whole (jd['final_results']['width_m'])),
		simple_td ('dataval', whole (jd['final_results']['octagon_area_m2'])),
		simple_td ('dataval', whole (jd['final_results']['mesh_area_m2'])),
		'</TR>',
		'<TR>',
		simple_td ('dataname', IMPORTCSV['coapp']),
		simple_td ('dataname', EXPORTCSV['s_to_r_grad']),
		simple_td ('dataname', EXPORTCSV['s_to_r_dist']),
		'</TR>',
		'<TR>',
		simple_td ('dataval', coapp),
		simple_td ('dataval', s_to_r_grad),
		simple_td ('dataval', distance_str),
		'</TR>',
		'<TR>',
		simple_td ('dataname', EXPORTCSV['r_elev']),
		simple_td ('dataname', IMPORTCSV['s_elev']),
		simple_td ('dataname', EXPORTCSV['prominence']),
		'</TR>',
		'<TR>',
		simple_td ('dataval', whole (r_elev)),
		simple_td ('dataval', s_elev_str),
		simple_td ('dataval', prominence),
		'</TR>',
		'<TR>',
		simple_td ('dataval str', footer_str, colspan = 3),
		'</TR>',
		'<TR>',
		simple_td ('dataval str', r_class, colspan = 3),
		'</TR>',
		'<TR>',
		'<TD colspan=3>',
		'<TABLE class="filler">',
		'<TR>',
		simple_td ('dataval str', geom_str, width = '37%'),
		simple_td ('dataval str', target_str),
		'</TR>',
		'</TABLE>',  # end of the 2-column irregular row table
		'</TD>',
		'</TR>',
		'<TR>',
		'<TD colspan=3>',
		'<TABLE class="filler">',
		'<TR>',
		simple_td ('dataval str', pid_str),
		simple_td ('dataval', fileid, width = '15%'),
		'</TR>',
		'</TABLE>',  # end of the 2-column irregular row table
		'</TD>',
		'</TR>',
		'</TABLE>',  # end of the data table
		'</TD>',
		'</TR>',
		'</TABLE>',  # end of the entry table
	]
	return "\n".join (tmp)


def gen_html_header() -> str:
	tmp = [
		'<HTML>',
		'<HEAD>',
		'<STYLE type="text/css">',
		CSSTEXT,
		'</STYLE>',
		'<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />',
		'</HEAD>',
		'<BODY>',
	]
	return "\n".join (tmp)


def gen_html_footer() -> str:
	tmp = [
		'</BODY>',
		'</HTML>',
	]
	return "\n".join (tmp)


def gen_interleaf (section_code: str) -> tuple:
	PARTS = [
		{
			'name': 'Berwickshire Results Data',
			'sections':
			{
				'BWK3.1': 'First Degree Co-appellatives',
				'BWK3.2': 'Second Degree Co-appellatives',
				'BWK3.3': 'Collocated Co-appellatives',
				'BWK3.4': 'Relief-features without Co-appellative Settlements',
				'BWK9': 'Unsuccessful Geomorphons',
			},
		},
		{
			'name': '<I>The Landscape of Place-Names</I> Results Data',
			'sections':
			{
				'LPN3.1': 'Domes and Platforms',
				'LPN3.2': 'Domes and Platforms (impacted &ndash; low)',
				'LPN3.3': 'Domes and Platforms (impacted &ndash; high)',
				'LPN3.4': 'Ridges and Banks',
				'LPN3.5': 'Ridges and Banks (impacted &ndash; low)',
				'LPN3.6': 'Ridges and Banks (impacted &ndash; high)',
				'LPN3.7': 'Headlands and Spits',
				'LPN3.8': 'Headlands and Spits (impacted &ndash; low)',
				'LPN3.9': 'Headlands and Spits (impacted &ndash; high)',
				'LPN3.10': 'Relief-features without Co-appellative Settlements',
				'LPN9': 'Unsuccessful Geomorphons',
			},
		},
	]
	for part in PARTS:
		if section_code in part['sections']:
			part_name = part['name']
			section_name = part['sections'][section_code]
			break
	else:
		part_name = 'unknown part'
		section_name = 'unknown section'
	tmp = [
		'<DIV class="part_name">%s</DIV>' % part_name,
		'<DIV class="section_name">%s</DIV>' % section_name,
		'<DIV class="section_code">(%s)</DIV>' % section_code,
	]
	return ("\n".join (tmp), False)


def gen_entries (csvdata: list) -> list:
	ret = []
	while len (csvdata):
		tmp = []
		for rowdata in csvdata[:ENTRIES_PER_PAGE]:
			jsonfilename = '%s/%s.json' % (JSONDIR, rowdata['name'])
			jsondata = load_profile_or_exit (open (jsonfilename, 'r'), 0, 9)
			tmp.append (gen_entry (rowdata, jsondata))
		ret.append (("\n".join (tmp), len (tmp) == ENTRIES_PER_PAGE))
		del csvdata[:ENTRIES_PER_PAGE]
	return ret


class WkHtmlToPdf (Thread):
	def __init__ (self, sem: BoundedSemaphore, html: str, pdffn: str):
		self.sem = sem
		self.input = html.encode()
		self.pdffn = pdffn
		super().__init__()
		self.start()

	def run (self):
		with self.sem:
			session.run_command (
				[
					'wkhtmltopdf',
					'--quiet',
					'--page-size', 'A4',
					'--orientation', 'Portrait',
					'-',
					self.pdffn,
				],
				input = self.input,
				quiet = True  # libpng warning: iCCP: known incorrect sRGB profile
			)


session = session()

sections = OrderedDict()
rowno = 1
try:
	for row in csv.reader (args.input):
		parsed = pnplib.parse_input_csv_row (row)
		pnplib.test_language (parsed['etymology'], parsed['element'])
		if parsed['section'] not in sections:
			sections[parsed['section']] = []
		# Trust generate_input_csv.py to ensure that the sections are contiguous.
		sections[parsed['section']].append (parsed)
		rowno += 1
except ValueError as ve:
	print ('CSV row %u: %s' % (rowno, str (ve)), file = sys.stderr)
	exit (os.EX_DATAERR)

html_pages = []
for k, section in sections.items():
	html_pages.append (gen_interleaf (k))
	html_pages.extend (gen_entries (section))

reams = []
pageno = args.first_page_no
while len (html_pages):
	html = [gen_html_header()]
	for page, with_pageno in html_pages[:PAGES_PER_PROC]:
		html.append ('<DIV class="page">%s<DIV class="footer">%s</DIV></DIV>' % \
			(page, pageno if with_pageno else ''))
		html.append ('<DIV class="pagebreak"></DIV>')
		pageno += 1
	html.pop()  # Omit the final page break.
	html.append (gen_html_footer())
	reams.append (('ream_%04u' % len (reams), "\n".join (html)))
	del html_pages[:PAGES_PER_PROC]

tmpfn = session.create_tmp_files ([(p, '.pdf') for p, h in reams])

unitecmd = ['pdfunite']
maxprocs = BoundedSemaphore (args.max_procs)
threads = []
for prefix, html in reams:
	threads.append (WkHtmlToPdf (maxprocs, html, tmpfn[prefix]))
	unitecmd.append (tmpfn[prefix])
unitecmd.append (args.output)
for t in threads:
	t.join()
session.run_command (unitecmd)
