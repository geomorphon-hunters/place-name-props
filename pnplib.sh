#!/bin/sh

# This file is a part of Place-Name Props, a collection of supplementary files
# for the Berwickshire Place-Name Resource project. See the accompanying file
# "COPYING" for the copyright and licensing information.

produce_file()
{
	PF_CMD=${1:?}
	PF_DESTFILE=${2:?}
	pf_ret=0
	if ! [ -s "$PF_DESTFILE" ]; then
		if [ -f "$PF_DESTFILE" ]; then
			echo "WARNING: The file '$PF_DESTFILE' already exists, but is empty." >&2
			return 1
		fi
		PF_TMPFILE=$(mktemp --tmpdir produce_file_XXXXXX)
		[ -z "$SQUELCH" ] && printf 'Producing '
		printf '%s... ' "$(basename "$PF_DESTFILE")"
		if $PF_CMD "$PF_TMPFILE"; then
			if cp "$PF_TMPFILE" "$PF_DESTFILE"; then
				[ -z "$SQUELCH" ] && echo 'done'
			else
				echo "ERROR: Failed to copy the result to '$PF_DESTFILE'" >&2
				pf_ret=4
			fi
		else
			echo "ERROR: helper command '$PF_CMD' failed!" >&2
			pf_ret=1
		fi
		rm -f "$PF_TMPFILE"
	fi
	return $pf_ret
}
