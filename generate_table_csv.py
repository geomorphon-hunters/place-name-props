#!/usr/bin/env python3

# This file is a part of Place-Name Props, a collection of supplementary files
# for the Berwickshire Place-Name Resource project. See the accompanying file
# "COPYING" for the copyright and licensing information.

import argparse
import sys
import os
import csv
from ngrtools import NGR1m
from GeomorphonProfiler.gplib import load_profile_or_exit, DIRS
import pnplib


ap = argparse.ArgumentParser ()
ap.add_argument (
	'--input',
	metavar = 'FILE',
	nargs = '?',
	type = argparse.FileType ('r'),
	default = sys.stdin,
	help = 'file to read the CSV data from ("-" for stdin)')
ap.add_argument (
	'--jsondir',
	metavar = 'DIR',
	required = True,
	type = str,
	help = 'the directory with JSON profiles')
ap.add_argument (
	'--output',
	metavar = 'FILE',
	nargs = '?',
	type = argparse.FileType ('w'),
	default = sys.stdout,
	help = 'file to append the CSV data to ("-" for stdout)')
args = ap.parse_args()
JSONDIR = os.path.abspath (args.jsondir)


def gen_output_csv_header() -> list:
	csvheader = list (pnplib.EXPORTCSV.values())
	for i in DIRS:
		csvheader.append (i + ' Rel. Elev. (m)')
	for i in DIRS:
		csvheader.append (i + ' Distance (m)')
	for i in DIRS:
		csvheader.append (i + ' Offset-E (m)')
		csvheader.append (i + ' Offset-N (m)')
	for i in DIRS:
		csvheader.append (i + ' NGR')
	return csvheader


def gen_output_csv_data (csv_in: dict, jsondata: dict) -> list:
	jid = jsondata['intermediate_data']
	jcp = jsondata['computation_parameters']
	jfr = jsondata['final_results']
	csvdata = []
	r_elev = jid['origin_elevation_m']
	distance, gradient, s_elev = pnplib.dist_grad_s_elev (csv_in, r_elev)
	csvdata.append ('' if distance is None else '%.2f' % distance)
	csvdata.append ('%.2f' % r_elev)
	csvdata.append ('%.2f' % jid['origin_easting'])
	csvdata.append ('%.2f' % jid['origin_northing'])
	csvdata.append (str (jfr['landform_cat']))
	csvdata.append (jfr['landform_code'])
	csvdata.append (str (jfr['landform_deviation']))
	csvdata.append (str (jid['ternary_498']))
	csvdata.append (str (jid['ternary_6561']))
	csvdata.append ('%.2f' % jfr['azimuth'])
	csvdata.append ('%.2f' % jfr['elongation'])
	csvdata.append ('%.2f' % jfr['width_m'])
	csvdata.append ('%.2f' % (jfr['elongation'] * jfr['width_m']))
	csvdata.append ('%.2f' % jfr['intensity_m'])
	csvdata.append ('%.2f' % jfr['exposition_m'])
	prominence = pnplib.prominence (jfr)
	csvdata.append ('' if prominence is None else '%.2f' % prominence)
	csvdata.append ('%.2f' % jfr['range_m'])
	csvdata.append ('%f' % jfr['variance'])
	csvdata.append ('%f' % jfr['extends'])
	csvdata.append ('%.2f' % jfr['octagon_perimeter_m'])
	csvdata.append ('%.2f' % jfr['octagon_area_m2'])
	csvdata.append ('%.2f' % jfr['mesh_perimeter_m'])
	csvdata.append ('%.2f' % jfr['mesh_area_m2'])
	csvdata.append ('' if gradient is None else '%.2f' % gradient)
	for i in DIRS:
		csvdata.append ('%.2f' % jid['rel_elevation_m'][i])
	for i in DIRS:
		csvdata.append ('%.2f' % jid['distance_m'][i])
	for i in DIRS:
		csvdata.append ('%.2f' % jid['offset_easting_m'][i])
		csvdata.append ('%.2f' % jid['offset_northing_m'][i])
	for i in DIRS:
		coords = (
			round (jid['easting'][i]),
			round (jid['northing'][i]),
		)
		csvdata.append (NGR1m.to_alphanumeric_str (coords))
	return csvdata


gpcols = len (pnplib.INPUTCSV)
rowno = 1
# Skip the first columns with the Geomorphon Profiler working data.
header_row = list (pnplib.IMPORTCSV.values())
header_row += gen_output_csv_header()
print (','.join (pnplib.quote_csv_row (header_row)), file = args.output)

for csvrow in csv.reader (args.input):
	try:
		row = pnplib.parse_input_csv_row (csvrow)
	except ValueError as ve:
		print ('CSV row %u: %s' % (rowno, str (ve)), file = sys.stderr)
		exit (os.EX_DATAERR)
	jsonfilename = '%s/%s.json' % (JSONDIR, row['name'])
	jsondata = load_profile_or_exit (open (jsonfilename, 'r'), 0, 9)
	data_row = csvrow[gpcols:]
	data_row += gen_output_csv_data (row, jsondata)
	print (','.join (pnplib.quote_csv_row (data_row)), file = args.output)
	rowno += 1
