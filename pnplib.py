# This file is a part of Place-Name Props, a collection of supplementary files
# for the Berwickshire Place-Name Resource project. See the accompanying file
# "COPYING" for the copyright and licensing information.

from collections import OrderedDict
import re
from math import sqrt


INPUTCSV = [
	'name',
	'ngr0_eas',
	'ngr0_nor',
	'ngr9_eas',
	'ngr9_nor',
]
SMARKER = '\N{BULLET}S\N{BULLET}'
RMARKER = '\N{BULLET}R\N{BULLET}'
IMPORTCSV = OrderedDict ([
	('ngr0_string', RMARKER),
	('ngr9_string', SMARKER),
	('s_name', '%s Name' % SMARKER),
	('s_elev', '%s alt. (m)' % SMARKER),
	('r_name', '%s Name' % RMARKER),
	('parish', 'Parish/County'),
	('etymology', 'Etymology'),
	('pid', 'BWKR ID'),
	('coapp', 'co-app.'),
	('class', 'Class'),
	('element', 'Element'),
	('section', 'Section'),
	('row order', 'Row Order'),
])
EXPORTCSV = OrderedDict ([
	('s_to_r_dist', '%sto%s dis. (m)' % (SMARKER, RMARKER)),
	('r_elev', '%s alt. (m)' % RMARKER),
	('CSV-only 10', 'Raster-E'),
	('CSV-only 20', 'Raster-N'),
	('CSV-only 30', 'G10 Number'),
	('CSV-only 40', 'G10 Code'),
	('CSV-only 50', 'Landform Deviation'),
	('CSV-only 60', 'G498 Number'),
	('CSV-only 70', 'G6561 Number'),
	('CSV-only 80', 'Azimuth'),
	('CSV-only 90', 'Elongation'),
	('width', 'wid. (m)'),
	('length', 'len. (m)'),
	('CSV-only 100', 'Intensity (m)'),
	('CSV-only 110', 'Exposition (m)'),
	('prominence', 'pro. (m)'),
	('CSV-only 120', 'Range (m)'),
	('CSV-only 130', 'Variance'),
	('CSV-only 140', 'Extends'),
	('octa_per', 'per. (m)'),
	('octa_area', 'sur. (m\N{SUPERSCRIPT TWO})'),
	('mesh_per', 'mesh per. (m)'),
	('mesh_area', 'mesh sur. (m\N{SUPERSCRIPT TWO})'),
	('s_to_r_grad', '%sto%s gra (%%)' % (SMARKER, RMARKER)),
	# More CSV-only columns follow, see gen_output_csv_header().
])


def parse_input_csv_row (row: list) -> list:
	found = len (row)
	expected = len (INPUTCSV) + len (IMPORTCSV)
	if found != expected:
		raise ValueError ('found %u columns (must be %u)' % (found, expected))
	return dict (zip (INPUTCSV + list (IMPORTCSV), row))


def quote_csv_row (row: list) -> list:
	return [(val if val.find (',') == -1 else '"%s"' % val) for val in row]


def distance (a: tuple, b: tuple) -> float:
	"""Return the distance between two plane points."""
	x1, y1 = a
	x1 = float (x1)
	y1 = float (y1)
	x2, y2 = b
	x2 = float (x2)
	y2 = float (y2)
	return sqrt ((x2 - x1) ** 2 + (y2 - y1) ** 2)


def dist_grad_s_elev (rd: dict, r_elev: float) -> tuple:
	"""Calculate three values that depend on settlement coordinates."""
	dist = None
	s_to_r_grad = None
	s_elev = None
	if rd['ngr9_string'] != '':
		dist = distance (
			(rd['ngr0_eas'], rd['ngr0_nor']),
			(rd['ngr9_eas'], rd['ngr9_nor'])
		)
		if rd['s_elev'] != '':
			s_elev = float (rd['s_elev'])
			if dist > 0.0:
				s_to_r_grad = (r_elev - s_elev) / dist * 100.0
	return (dist, s_to_r_grad, s_elev)


def prominence (fr: dict) -> float:
	"""Return peak prominence given profile data final results."""
	return fr['exposition_m'] if fr['landform_code'] == 'PK' else None


def test_language (etymology: str, element: str):
	if etymology == '':
		EnglishTargetElement (element)
	else:
		tgtelem = ScotsTargetElement (element)
		ScotsEtymology (etymology, tgtelem)


class Token:
	def __init__ (self, s: str):
		self.text = self.html = s

	def __str__ (self) -> str:
		return self.text

	def to_html (self) -> str:
		return self.html


class Element (Token):
	def __init__ (self, s: str, valid_langs: list, valid_senses: list):
		s = re.sub (r'^\s*(.+?)\s*$', r'\1', s)  # trim
		s = re.sub (r'\s', ' ', s)  # normalize whitespace
		m = re.match (r'^([A-Za-z/1-3]+) ((?:\?)|(?:\*?[\w() -]+?))([0-9\?]*)$', s)
		if m is None:
			raise ValueError ('invalid element string "%s"' % s)
		self.lang, self.term, self.sense = m.groups()
		if self.lang not in valid_langs:
			raise ValueError ('invalid language "%s"' % self.lang)
		if self.sense != '' and self.sense not in valid_senses:
			raise ValueError ('invalid sense code "%s"' % self.sense)
		self.text = '%s %s%s' % (self.lang, self.term, self.sense)
		self.set_html ('element')

	def make_target (self):
		if self.term == '?':
			raise ValueError ('this element cannot have term "?"')
		self.set_html ('element target')

	def set_html (self, c: str):
		self.html = '<SPAN class="%s">' % c
		self.html += '<SPAN class="lang">%s</SPAN> ' % self.lang
		if self.term == '?':
			self.html += self.term
		else:
			self.html += '<SPAN class="term">%s</SPAN>' % self.term
		if len (self.sense) > 0:
			self.html += '<SPAN class="sense">%s</SPAN>' % self.sense
		self.html += '</SPAN>'

	def __eq__ (self, what) -> bool:
		return self.lang == what.lang and self.term == what.term and \
			self.sense == what.sense


class EnglishTargetElement (Element):
	def __init__ (self, s: str):
		LANGS = [
			'Britt',
			'OE',
			'PrCumb',
			'PrW'
		]
		SENSES = ['1', '2', '3', '1?', '2?', '3?', '?']
		super().__init__ (s, LANGS, SENSES)
		self.make_target()


class ScotsElement (Element):
	def __init__ (self, s: str):
		LANGS = [
			'Br1', 'Br2', 'Br3',        # Brittonic
			'ESc1', 'ESc2', 'ESc3',     # Early Scots
			'G1', 'G2', 'G3',           # Gaelic
			'OE1', 'OE2', 'OE3',        # Old English
			'ON1', 'ON2', 'ON3',        # Old Norse
			'Sc1', 'Sc2', 'Sc3',        # Scots
			'SSE1', 'SSE2', 'SSE3',     # Scottish Standard English
			'MSc1', 'MSc2', 'MSc3',     # Middle Scots
			'OE/Sc1', 'OE/ESc1',        # mixed
			'en', 'en1', 'en2', 'en3',  # existing name
			'pn1', 'pn2', 'pn3',        # personal name
			'un1'                       # unknown
		]
		SENSES = ['1', '2', '3']
		super().__init__ (s, LANGS, SENSES)


class ScotsTargetElement (ScotsElement):
	def __init__ (self, s: str):
		super().__init__ (s)
		if self.sense == '':
			raise ValueError ('a target element must have a sense code')
		self.make_target()


class ScotsEtymology:
	def __init__ (self, tokseq: str, target = None):
		target_present = False
		self.tokens = list()
		for each in re.split (r'(\+)|\s(or)\s', tokseq):
			if each is None:
				continue
			elif each == '+' or each == 'or':
				self.tokens.append (Token (each))
			else:
				element = ScotsElement (each)
				if target is not None and target == element:
					self.tokens.append (target)
					target_present = True
				else:
					self.tokens.append (element)
		if target is not None and not target_present:
			raise ValueError ('the target element "%s" does not appear in "%s"' % \
				(target, self)
			)

	def __str__ (self) -> str:
		return ' '.join ([str (each) for each in self.tokens])

	def to_html (self) -> str:
		ret = '<SPAN>'
		ret += ' '.join ([each.to_html() for each in self.tokens])
		ret += '</SPAN>'
		return ret
