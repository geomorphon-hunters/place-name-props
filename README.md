# [Place-Name Props](https://gitlab.com/geomorphon-hunters/place-name-props)

This is a collection of scripts to support the [Hill-terms in the place-names
of Berwickshire](https://theses.gla.ac.uk/82861/) project. The scripts are
free software licensed under GPLv3. The Python code has been tested to work
with Python versions 3.6 and 3.8.

## Geomorphon Profiler interface
The following scripts translate data between the project problem space and the
[Geomorphon Profiler](https://gitlab.com/geomorphon-hunters/GeomorphonProfiler)
solution space:

* `bootstrap.sh` — the initial setup script (to be run manually).
* `import.sh` — the data import script (to be run manually after preparing
  `config.sh` in Geomorphon Profiler), which produces `input.csv`
  ([example](examples/import.csv)) and enables subsequent data processing.
* `export.sh` — the data export script (to be run automatically by the
  Geomorphon Profiler's `./start.sh -e` command), which produces CSV
  ([example](examples/export.csv)) and PDF ([example](examples/export.pdf))
  versions of the computation results; it depends on `wkhtmltopdf` and
  `pdfunite` (poppler-utils). This script needs either a symlink in the
  Geomorphon Profiler directory, or an `EXPORT_SCRIPT=/path/to/export.sh`
  line in the `config.sh` file.

## NGR 1m grid square translator
`ngrtools.py` is a helper module to translate
[British National Grid](https://en.wikipedia.org/wiki/Ordnance_Survey_National_Grid)
1m grid square coordinates between numeric and alphanumeric notations, for
example:
```text
# Arthur's Seat
$ ./ngrtools.py --num2alnum 327533,672946
NT 27533 72946
# Ben Nevis
$ ./ngrtools.py --alnum2num='NN 16600 71200'
216600,771200
```

## How to use
1. (Linux PC) Run the following command to remove any previous data:
   ```sh
   start.sh -d ALL && rm ~/input.csv
   ```
2. (Windows PC) Prepare the `import.xlsx` spreadsheet, making sure the rows
   and the columns follow the agreed order and there are no header rows.
3. (Windows PC) Save the spreadsheet as `import.csv`, making sure the file
   type is set to "CSV UTF-8 (Comma-delimited)".
4. (Windows PC) Copy `import.csv` into the `~/place-name-props` directory on
   the Linux PC (overwrite the existing file if present).
5. (Linux PC) Run the following command and confirm success:
   ```sh
   ~/place-name-props/import.sh
   ```
6. (Linux PC) Run the following command to start the data processing:
   ```sh
   start.sh -e
   ```
7. (Linux PC) Upon completion locate the results within the `~/gp_output`
   directory.
8. (Windows PC) Copy `export.csv`, `export.pdf` and any other required files
   from the Linux PC.

## Credits
* Dàibhidh Grannd, English Language and Linguistics, School of Critical
  Studies, University of Glasgow, 2020-2021 — the original scientific idea
  and testing.
* Denis Ovsienko, 2020-2021 — technical design and implementation.
* GRASS Development Team, 2022. [Geographic Resources Analysis Support
  System (GRASS) Software](https://grass.osgeo.org), Version 8.0. Open Source
  Geospatial Foundation.
* The 30m DEM data used to generate the example results have been provided by
  [ALOS World 3D](https://www.eorc.jaxa.jp/ALOS/en/aw3d/index_e.htm) of the
  [Japan Aerospace Exploration Agency](https://global.jaxa.jp/).
* Ashish Kulkarni and Jakob Truelsen — wkhtmltopdf command line tool.
* freedesktop.org — Poppler command line tools.
