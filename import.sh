#!/bin/sh

# This file is a part of Place-Name Props, a collection of supplementary files
# for the Berwickshire Place-Name Resource project. See the accompanying file
# "COPYING" for the copyright and licensing information.

MYDIR=$(dirname "$(realpath "$0")")
SCRIPTDIR="$MYDIR/GeomorphonProfiler"
# shellcheck disable=SC1090
SHALLOW_INIT=yes . "$SCRIPTDIR/init.sh"
require_grass_session

if [ -s "$INPUTFILE" ]; then
	echo "ERROR: The file '$INPUTFILE' already exists!" >&2
	exit 1
fi

# shellcheck source=./pnplib.sh
. "$MYDIR/pnplib.sh"
EXTERNALFILE=${1:-$MYDIR/import.csv}
assert_nonempty_file "$EXTERNALFILE"

gen_input_file()
{
	sed '1s/^\xEF\xBB\xBF//' < "$EXTERNALFILE" |
		tr -d '\r' |
		"$MYDIR/generate_input_csv.py" \
			--elevation="$ELEVATION" \
			--output="${1:?}"
}

produce_file gen_input_file "$INPUTFILE"
