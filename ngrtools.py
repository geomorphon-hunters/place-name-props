#!/usr/bin/env python3

# This file is a part of Place-Name Props, a collection of supplementary files
# for the Berwickshire Place-Name Resource project. See the accompanying file
# "COPYING" for the copyright and licensing information.

import re
from typing import Tuple
ParsedCoords = Tuple[int, int]


class NGR1m:
	# Row index ascends as the actual grid northing ascends.
	GRID = [
		['SV', 'SW', 'SX', 'SY', 'SZ', 'TV', 'TW'],
		['SQ', 'SR', 'SS', 'ST', 'SU', 'TQ', 'TR'],
		['SL', 'SM', 'SN', 'SO', 'SP', 'TL', 'TM'],
		['SF', 'SG', 'SH', 'SJ', 'SK', 'TF', 'TG'],
		['SA', 'SB', 'SC', 'SD', 'SE', 'TA', 'TB'],
		['NV', 'NW', 'NX', 'NY', 'NZ', 'OV', 'OW'],
		['NQ', 'NR', 'NS', 'NT', 'NU', 'OQ', 'OR'],
		['NL', 'NM', 'NN', 'NO', 'NP', 'OL', 'OM'],
		['NF', 'NG', 'NH', 'NJ', 'NK', 'OF', 'OG'],
		['NA', 'NB', 'NC', 'ND', 'NE', 'OA', 'OB'],
		['HV', 'HW', 'HX', 'HY', 'HZ', 'JV', 'JW'],
		['HQ', 'HR', 'HS', 'HT', 'HU', 'JQ', 'JR'],
		['HL', 'HM', 'HN', 'HO', 'HP', 'JL', 'JM'],
	]

	@classmethod
	def square_origin (cls, square_name: str) -> ParsedCoords:
		"""Tell the south-west corner coordinates for the given 100km square.

		Check that the square belongs to the defined grid.
		"""
		northing = 0
		for row in cls.GRID:
			easting = 0
			for column in row:
				if column == square_name:
					return (easting, northing)
				easting += 100000
			northing += 100000
		raise ValueError ('square %s is not on this grid' % square_name)

	@classmethod
	def square_name (cls, coords: ParsedCoords) -> str:
		"""Tell the 100km square name for the given coordinates.

		Check that the coordinates are valid.
		"""
		easting, northing = coords
		row = int (northing / 100000)
		if northing < 0 or row >= len (cls.GRID):
			raise ValueError ('northing %d out of range' % northing)
		col = int (easting / 100000)
		if easting < 0 or col >= len (cls.GRID[row]):
			raise ValueError ('easting %d out of range' % easting)
		return cls.GRID[row][col]

	@classmethod
	def from_numeric_str (cls, string: str) -> ParsedCoords:
		"""Convert a numeric format string to an (easting, northing) tuple.

		Check that the format and the coordinates are valid.
		"""
		patterns = [
			'^([0-9]+) ([0-9]+)$',
			'^([0-9]+),([0-9]+)$',
			'^([0-9]+) ([0-9]+)$',
			'^([0-9]+), ([0-9]+)$',
		]
		for pattern in patterns:
			m = re.match (pattern, string)
			if m is not None:
				easting = int (m.group (1))
				northing = int (m.group (2))
				cls.square_name ((easting, northing))
				return (easting, northing)
		else:
			raise ValueError ('invalid numeric coordinates "%s"' % string)

	@classmethod
	def from_alphanumeric_str (cls, string: str) -> ParsedCoords:
		"""Convert an alphanumeric format string to an (easting, northing) tuple.

		Check that the format and the coordinates are valid.
		"""
		patterns = [
			'^([A-Z]{2}) ([0-9]{5}) ([0-9]{5})$',  # either both spaces
			'^([A-Z]{2})([0-9]{5})([0-9]{5})$',    # or none
		]
		for pattern in patterns:
			m = re.match (pattern, string)
			if m is not None:
				square_name, rel_easting, rel_northing = m.groups()
				so_easting, so_northing = cls.square_origin (square_name)
				return (so_easting + int (rel_easting), so_northing + int (rel_northing))
		else:
			raise ValueError ('invalid alphanumeric coordinates "%s"' % string)

	@staticmethod
	def to_numeric_str (coords: ParsedCoords) -> str:
		"""Format an (easting, northing) tuple as numeric coordinates."""
		return '%u,%u' % coords

	@classmethod
	def to_alphanumeric_str (cls, coords: ParsedCoords) -> str:
		"""Format an (easting, northing) tuple as alphanumeric coordinates."""
		easting, northing = coords
		return '%s %05u %05u' % (
			cls.square_name (coords),
			easting % 100000,
			northing % 100000
		)


if __name__ == '__main__':
	import sys
	import argparse

	def argparse_validate_alphanumeric (string: str) -> ParsedCoords:
		try:
			return NGR1m.from_alphanumeric_str (string)
		except ValueError as ve:
			raise argparse.ArgumentTypeError (str (ve)) from ve

	def argparse_validate_numeric (string: str) -> ParsedCoords:
		try:
			return NGR1m.from_numeric_str (string)
		except ValueError as ve:
			raise argparse.ArgumentTypeError (str (ve)) from ve

	ap = argparse.ArgumentParser (
		description = 'Convert NGR 1m square coordinates format from \
		alphanumeric to numeric and vice versa. A direction is mandatory; if \
		the coordinates are omitted, every line of the standard input will \
		be translated. This script exits on the first incorrect input value.')
	meg = ap.add_mutually_exclusive_group (required = True)
	meg.add_argument (
		'--alnum2num',
		nargs = '?',
		const = False,  # if given with no value
		metavar = 'ALNUM',
		type = argparse_validate_alphanumeric,
		help = 'from "NT 27533 72946" to "327533,672946"')
	meg.add_argument (
		'--num2alnum',
		nargs = '?',
		const = False,  # if given with no value
		metavar = 'NUM',
		type = argparse_validate_numeric,
		help = 'from "327533,672946" to "NT 27533 72946"')
	args = ap.parse_args()
	if args.alnum2num is not None:
		coords = args.alnum2num
		from_func = NGR1m.from_alphanumeric_str
		to_func = NGR1m.to_numeric_str
	elif args.num2alnum is not None:
		coords = args.num2alnum
		from_func = NGR1m.from_numeric_str
		to_func = NGR1m.to_alphanumeric_str
	else:
		ap.error ('this is a bug')
	if coords is not False:
		# A one-off conversion.
		print (to_func (coords))
	else:
		# A pipe conversion.
		for line in sys.stdin:
			print (to_func (from_func (line.rstrip ("\n"))))
