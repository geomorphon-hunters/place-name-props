#!/usr/bin/env python3

# This file is a part of Place-Name Props, a collection of supplementary files
# for the Berwickshire Place-Name Resource project. See the accompanying file
# "COPYING" for the copyright and licensing information.

import argparse
import sys
import os
import csv
from ngrtools import NGR1m
from grass.script.core import run_command, read_command
import pnplib


ap = argparse.ArgumentParser ()
ap.add_argument (
	'--input',
	metavar = 'FILE',
	nargs = '?',
	type = argparse.FileType ('r'),
	default = sys.stdin,
	help = 'file to read the CSV data from ("-" for stdin)')
ap.add_argument (
	'--elevation',
	metavar = 'RASTER',
	required = True,
	help = 'r.geomorphon elevation raster name')
ap.add_argument (
	'--output',
	metavar = 'FILE',
	nargs = '?',
	type = argparse.FileType ('a'),
	default = sys.stdout,
	help = 'file to append the CSV data to ("-" for stdout)')
args = ap.parse_args()


run_command ('g.region', raster = args.elevation)
rowno = 0
errors = 0
TRAILER_ROW = [''] * len (pnplib.IMPORTCSV)
seen_trailer = False
seen_sections = []
prev_section = None
for row_in in csv.reader (args.input):
	rowno += 1
	if row_in == TRAILER_ROW:
		seen_trailer = True
		continue
	try:
		if seen_trailer:
			raise ValueError ('Empty row!')
		test = pnplib.parse_input_csv_row (pnplib.INPUTCSV + row_in)
		if test['section'] != prev_section:
			prev_section = test['section']
			if test['section'] in seen_sections:
				raise ValueError ('Section "%s" begins again!' % test['section'])
			seen_sections.append (test['section'])
		pnplib.test_language (test['etymology'], test['element'])
		ngr0_string = row_in.pop (0)
		ngr0_alnum = ngr0_string.lstrip ('*')
		ngr0_eas, ngr0_nor = NGR1m.from_alphanumeric_str (ngr0_alnum)
		ngr9_string = row_in.pop (0)
		if ngr9_string == '':
			ngr9_eas = ngr9_nor = ''
		else:
			ngr9_eas, ngr9_nor = \
				NGR1m.from_alphanumeric_str (ngr9_string.lstrip ('*'))
	except ValueError as ve:
		print ('CSV row %u: %s' % (rowno, str (ve)), file = sys.stderr)
		errors += 1
		if errors < 20:
			continue
		else:
			print ('Too many errors, terminating early!', file = sys.stderr)
			break
	s_name = row_in.pop (0)
	s_elev = row_in.pop (0)
	if s_elev == '' and ngr9_string != '':
		tmp = read_command (
			'r.what',
			map = args.elevation,
			coordinates = '%u,%u' % (ngr9_eas, ngr9_nor)
		)
		tmp = tmp.rstrip ("\n")
		tmp = tmp.split ('|')
		s_elev = tmp[3]
	row_out = [
		# Offset for the missing header row.
		'%04u_%s' % (rowno + 1, ngr0_alnum.replace (' ', '_')),
		str (ngr0_eas),
		str (ngr0_nor),
		str (ngr9_eas),
		str (ngr9_nor),
		ngr0_string,
		ngr9_string,
		s_name,
		s_elev,
	] + row_in
	print (','.join (pnplib.quote_csv_row (row_out)), file = args.output)
if errors:
	print ('Exiting due to input data errors :-P', file = sys.stderr)
	exit (os.EX_DATAERR)
