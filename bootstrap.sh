#!/bin/sh -e

MYDIR=$(dirname "$(realpath "$0")")
git -C "$MYDIR" submodule update --init
if ! [ -e "$MYDIR/GeomorphonProfiler/export.sh" ]; then
	ln -s ../export.sh "$MYDIR/GeomorphonProfiler/export.sh"
fi
